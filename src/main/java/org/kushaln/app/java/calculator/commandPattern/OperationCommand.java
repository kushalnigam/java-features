package org.kushaln.app.java.calculator.commandPattern;

public interface OperationCommand {

	Integer execute();
}
