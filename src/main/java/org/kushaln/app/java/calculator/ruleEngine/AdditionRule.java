package org.kushaln.app.java.calculator.ruleEngine;

import org.kushaln.app.java.util.Operators;

public class AdditionRule implements Rule {

	private Integer result;

	@Override
	public boolean evaluate(RuleExpression ruleExpression) {
		if (ruleExpression.getOperator() == Operators.ADD) {
			this.result = ruleExpression.getField1() + ruleExpression.getField2();
			return true;
		}
		return false;
	}

	@Override
	public Result getEvaluationResult() {
		return Result.builder().resultInt(result).build();
	}

}
