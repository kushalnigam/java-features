package org.kushaln.app.java.calculator.simple;

import org.kushaln.app.java.util.Operators;

public class SimpleCalculator {

	public int calculate(int a, int b, String operator) {
		int result = Integer.MIN_VALUE;
		if (operator.equalsIgnoreCase("add")) {
			result = a + b;
		} else if (operator.equalsIgnoreCase("subtract")) {
			result = a - b;
		} else if (operator.equalsIgnoreCase("multiply")) {
			result = a * b;
		} else if (operator.equalsIgnoreCase("divide")) {
			result = a / b;
		}
		return result;
	}
	
	public int calculate(int a, int b, Operators operator) {
		int result = Integer.MIN_VALUE;
		if (Operators.ADD.equals(operator)) {
			result = a + b;
		} else if (Operators.SUBTRACT.equals(operator)) {
			result = a - b;
		} else if (Operators.MULTIPLICATION.equals(operator)) {
			result = a * b;
		} else if (Operators.DIVISION.equals(operator)) {
			result = a / b;
		}
		return result;
	}
	
	public int calculate_viaSwitch(int a, int b, Operators operator) {
		int result = Integer.MIN_VALUE;
		switch (operator) {
		case ADD:
			result = a + b;
			break;
		case SUBTRACT:
			result = a - b;
			break;
		case MULTIPLICATION:
			result = a - b;
			break;
		case DIVISION:
			result = a - b;
			break;
		default:
			throw new IllegalArgumentException("Invalid Operator");
		}
		return result;
	}
}
