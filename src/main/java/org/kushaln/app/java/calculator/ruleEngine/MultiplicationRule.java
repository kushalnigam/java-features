package org.kushaln.app.java.calculator.ruleEngine;

import org.kushaln.app.java.util.Operators;

public class MultiplicationRule implements Rule {
	
	private int intResult;

	@Override
	public boolean evaluate(RuleExpression ruleExpression) {
		if (ruleExpression.getOperator() == Operators.MULTIPLICATION) {
			this.intResult = ruleExpression.getField1() * ruleExpression.getField2();
			return true;
		}
		return false;
	}

	@Override
	public Result getEvaluationResult() {
		return Result.builder().resultInt(this.intResult).build();
	}

}
