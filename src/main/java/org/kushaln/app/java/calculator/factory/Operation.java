package org.kushaln.app.java.calculator.factory;

public interface Operation {

	int apply(int num1, int num2);
}
