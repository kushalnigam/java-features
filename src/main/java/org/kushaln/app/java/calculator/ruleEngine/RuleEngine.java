package org.kushaln.app.java.calculator.ruleEngine;

import java.util.ArrayList;
import java.util.List;

public class RuleEngine {

	private static List<Rule> rules = new ArrayList<>();

	static {
		rules.add(new AdditionRule());
		rules.add(new MultiplicationRule());
	}

	public static Result process(RuleExpression expression) {
		Rule rule = rules.stream().filter(r -> r.evaluate(expression)).findFirst()
				.orElseThrow(() -> new IllegalArgumentException("Expression does not matches any Rule"));
		return rule.getEvaluationResult();
	}
}
