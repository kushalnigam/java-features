package org.kushaln.app.java.calculator.ruleEngine;

import org.kushaln.app.java.util.Operators;

public class RuleExpression {

	private Operators operator;
	private Integer field1;
	private Integer field2;

	public RuleExpression(Operators operator, Integer field1, Integer field2) {
		super();
		this.operator = operator;
		this.field1 = field1;
		this.field2 = field2;
	}

	public Operators getOperator() {
		return operator;
	}

	public Integer getField1() {
		return field1;
	}

	public Integer getField2() {
		return field2;
	}

}
