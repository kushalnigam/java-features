package org.kushaln.app.java.calculator.commandPattern;

/**
 * @author e094022
 *
 * Adv: The command can defines its own variables required for execution of itself. Eg Add need two numbers.
 */
public class AdditionCommand implements OperationCommand {

	private int num1;
	private int num2;

	public AdditionCommand(int num1, int num2) {
		super();
		this.num1 = num1;
		this.num2 = num2;
	}

	@Override
	public Integer execute() {
		return num1 + num2;
	}

}
