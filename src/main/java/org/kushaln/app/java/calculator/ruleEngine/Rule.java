package org.kushaln.app.java.calculator.ruleEngine;

public interface Rule {

	boolean evaluate(RuleExpression ruleExpression);
	
	Result getEvaluationResult();
}
