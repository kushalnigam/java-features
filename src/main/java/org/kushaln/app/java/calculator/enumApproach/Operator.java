package org.kushaln.app.java.calculator.enumApproach;

public enum Operator {

	ADD {
		@Override
		public int apply(int num1, int num2) {
			return num1 + num2;
		}
	},
	MULTIPLICATION {
		@Override
		public int apply(int num1, int num2) {
			return num1 * num2;
		}
	},
	SUBSTRACTION {
		@Override
		public int apply(int num1, int num2) {
			return num1 - num2;
		}
	};
	public abstract int apply(int num1, int num2);
}
