package org.kushaln.app.java.calculator.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.kushaln.app.java.util.Operators;

/**
 * @author Nigam, Kushal <br>
 *
 * Provides Operation instances - Add, Multiply, etc.
 */
public class OperatorFactory {

	private static Map<Operators, Operation> operations = new HashMap<>();
	
	static {
		operations.put(Operators.ADD, new AddOperation());
		operations.put(Operators.MULTIPLICATION, new MultipliyOperation());
	}
	
	public static Optional<Operation> getOperator(Operators operator) {
		return Optional.ofNullable(OperatorFactory.operations.get(operator));
	}
	
	public static void addOperationtoFactory(Operators operatorKey, Operation operation) {
		operations.put(operatorKey, operation);
	}
}
