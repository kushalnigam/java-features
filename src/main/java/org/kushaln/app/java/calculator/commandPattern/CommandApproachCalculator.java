package org.kushaln.app.java.calculator.commandPattern;

public class CommandApproachCalculator {

	public int calculate(OperationCommand command) {
		return command.execute();
	}
}
