package org.kushaln.app.java.calculator.ruleEngine;

import lombok.Builder;
import lombok.Data;

@Data @Builder
public class Result {

	private Integer resultInt;
}
