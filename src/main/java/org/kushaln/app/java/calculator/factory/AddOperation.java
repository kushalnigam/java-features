package org.kushaln.app.java.calculator.factory;

public class AddOperation implements Operation {

	public int apply(int num1, int num2) {
		return num1 + num2;
	}

}
