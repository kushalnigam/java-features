package org.kushaln.app.java;

import org.kushaln.app.java.calculator.commandPattern.AdditionCommand;
import org.kushaln.app.java.calculator.commandPattern.CommandApproachCalculator;
import org.kushaln.app.java.calculator.enumApproach.Operator;
import org.kushaln.app.java.calculator.factory.Operation;
import org.kushaln.app.java.calculator.factory.OperatorFactory;
import org.kushaln.app.java.calculator.ruleEngine.RuleEngine;
import org.kushaln.app.java.calculator.ruleEngine.RuleExpression;
import org.kushaln.app.java.calculator.simple.SimpleCalculator;
import org.kushaln.app.java.util.Operators;


/**
 * @author e094022
 *
 * Source: https://www.baeldung.com/java-replace-if-statements
 */
public class ApplicationRunner {

	public static void main(String[] args) {
		System.out.println("----------- Application Started ------------");
		testSimpleCalculator();
		testFactoryCalculator();
		testEnumCalculator();
		testCommandPatternCalculator();
		testRuleEngineBasedCalculator();
	}

	static void testSimpleCalculator() {
		SimpleCalculator calculator = new SimpleCalculator();
		int var1 = 2, var2 = 3;
		//int result = calculator.calculate(var2, var1, "subtract");
		int result1 = calculator.calculate(var2, var1, Operators.ADD);
		System.out.println("Result[SimpleCalculator] = " + result1);
	}
	
	static void testFactoryCalculator() {
		//Operation addOperation = OperatorFactory.getOperator(Operators.MULTIPLICATION);
		int var1 = 2, var2 = 3;
		Operation targetOperation = OperatorFactory.getOperator(Operators.MULTIPLICATION).orElseThrow(() -> new IllegalArgumentException("Invalid Operation."));
		int result = targetOperation.apply(var1, var2);
		System.out.println("Result[OperationFactory] = " + result);
	}
	
	static void testEnumCalculator() {
		int var1 = 2, var2 = 3;
		int result1 = Operator.MULTIPLICATION.apply(var1, var2);
		int result = Operator.valueOf("MULTIPLICATION").apply(var1, var2);
		System.out.println("Result[EnumCalculator] = " + result);
	}
	
	/**
	 * Diff CommandPattern Vs Factory Method: Factory method was used to get
	 * Operation Instance/Command, whereas command objects are passed to the
	 * Business interface and called by the BusinessService impl.
	 */
	static void testCommandPatternCalculator() {
		int var1 = 2, var2 = 3;
		CommandApproachCalculator calc = new CommandApproachCalculator();
		int result = calc.calculate(new AdditionCommand(var1, var2));
		System.out.println("Result[Command Pattern Calculator] = " + result);
	}
	
	static void testRuleEngineBasedCalculator() {
		int var1 = 2, var2 = 3;
		RuleExpression addExpression = new RuleExpression(Operators.MULTIPLICATION, var1, var2);
		int result = RuleEngine.process(addExpression).getResultInt();
		System.out.println("Result[Rule Engine Based Calculator] = " + result);
	}
}
